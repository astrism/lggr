var Lggr = {};
Lggr.height = 200;
Lggr.titleHeight = 20;
Lggr.open = true;
Lggr.filterStrength = 20; //higher value smooths out reporting
Lggr.frameTime = 0;
Lggr.lastLoop = new Date;
Lggr.pos = 0;
Lggr.logger;
Lggr.allLogs = [];
Lggr.html = 
"<div id='lggr' class='lggrTouch'>"+
	"<ul id='lggrScroll'></ul>"+
	"<div id='lggrTitle' class='lggrNoTouch'>"+
		"<div id='lggrOpen'></div>"+
		"<span class='lggrLeft'>"+
			"<span class='lggrTouch'>"+
				"<input class='lggrTouch' id='lggrRunInput' type='text' value='' size='20' autocorrect='off' autocapitalization='off' placeholder='Run'></input>"+
				"<div id='lggrFormClick'></div>"+
			"</span>"+
			"<span class='lggrTouch'>"+
				"<input class='lggrTouch' id='lggrFilterInput' type='text' value='' size='20' autocorrect='off' autocapitalization='off' placeholder='Filter'></input>"+
				"<div id='lggrInputClick'></div>"+
			"</span>"+
			"<span id='lggrFps'>0 fps</span>"+
		"</span>"+
		"<span class='lggrRight lggrTouch'>"+
			"<span id='lggrKill'>CLOSE</span>"+
			"<span id='lggrExpand'>UP</span>"+
			"<span id='lggrContract'>DN</span>"+
		"</span>"+
	"</div>"+
"</div>";

Lggr.floatLog = function() {
	if($("#lggr") != null)
		$("body").append($("#lggr"));
}

function log(param)
{
	window.console.log(param);
	
	var loggerScroll = $("#lggrScroll");
	var logs = "<ul class='lggrLogs'><li>" + String(param) + "</li>";
	for(var i = 1; i < arguments.length; i++)
	{
		window.console.log(arguments[i]);
		logs += "<li>" + arguments[i] + "</li>";
	}

	logs += "</ul>";
	
	var index = Lggr.allLogs.length;
	var logged = "<li class='lggrLog'><span>" + index + "</span>" + logs + "</li>";
	
	var filter = $("#lggrFilterInput").val();
	if(Lggr.open && (filter.length > 0 && logged.indexOf(filter) > -1))
	{
		$("#lggrScroll").prepend(logged);
	}
	Lggr.allLogs.push(logged);
}


$(function ()
{
	$('body').append(Lggr.html);
	Lggr.logger = $("#lggr");
	var loggerScroll = $('#lggrScroll');
	var loggerRunInput = $("#lggrRunInput");
	var loggerFilterInput = $("#lggrFilterInput");
	
	loggerScroll.css('height', Lggr.height);
	$('#lggrTitle').css('height', Lggr.titleHeight);
	loggerRunInput.bind('keypress', inputEntry);
	loggerFilterInput.bind('keypress', inputEntry);
	$('#lggrFormClick').bind('click', inputFocus);
	$('#lggrOpen').bind('click', toggleLog);
	$('#lggrKill').bind('click', killLog);
	$('#lggrExpand').bind('click', expand);
	$('#lggrContract').bind('click', contract);
	$('#lggrFilter').bind('click', filterLogs);
	
	Lggr.pos = Lggr.height;
	loggerScroll.css('padding-bottom', Lggr.titleHeight);
	loggerScroll.css('bottom', -Lggr.pos);
	toggleLog();
	initFPS();
	log("--- log ---");

	function toggleLog() 
	{
		if(Lggr.open == false) {
			Lggr.open = true;
			loggerScroll.css('bottom', 0);
			loggerScroll.css('height', Lggr.pos);
			renderLogs();
		} else {
			Lggr.open = false;
			loggerScroll.css('bottom', -Lggr.pos - Lggr.titleHeight);
			$("#lggrScroll").empty();
		}
	}
	
	function renderLogs() {
		var allLogs = [].concat(Lggr.allLogs);
		var filter = loggerFilterInput.val();
		var displayLogs = "";
		$.each(allLogs, function(i, v) {
			if(filter.length == 0 || v.indexOf(filter) > -1)
				displayLogs = v + displayLogs;
		});
		loggerScroll.prepend(displayLogs);
	}
	
	function filterLogs()
	{
		loggerFilterInput.val();
	}

	function killLog()
	{
		Lggr.logger.hide();
	}
	
	function inputEntry(event) {
		if(event.keyCode == 13) {
			switch(event.currentTarget.id)
			{
				case "lggrRunInput" :
					runEval();
					break;
				case "lggrFilterInput" :
					filterEval();
					break;
				default :
					log('Lggr: unhandled input');
					break;
			}
			return true;
		} else
			return event.which;
	}

	function runEval() {
		try {
			log(eval(loggerRunInput.val()));
		} catch (e){
			log(e);
			loggerRunInput.attr('placeholder', 'ERROR');
			$('#lggrFormClick').addClass('error');
		}	
		loggerRunInput.val('');
	}
	
	function filterEval() {
		$("#lggrScroll").empty();
		renderLogs();
	}
	
	function inputFocus() {
		loggerRunInput.focus();
		$('#lggrFormClick').removeClass('error');
	}

	function expand() {
		Lggr.pos += Lggr.height;
		if(Lggr.open)
			loggerScroll.css('height', Lggr.pos);
	}


	function contract() {
		if(Lggr.pos - Lggr.height > 0)
			Lggr.pos -= Lggr.height;
		if(Lggr.open)	
			loggerScroll.css('height', Lggr.pos);
	}
	
	function initFPS()
	{
		var onEachFrame;
		var requestFrame;
		if (window.webkitRequestAnimationFrame)//Chrome
			requestFrame = webkitRequestAnimationFrame;
		else if(window.requestAnimationFrame)
			requestFrame = requestAnimationFrame;

		if (requestFrame) {//this is Chrome only right now
				onEachFrame = function(cb) {
					var _cb = function() { cb(); requestFrame(_cb); };
					_cb();
				};
		} else {
			onEachFrame = function(cb) {
				setInterval(cb, 1000 / 60);
			};
		}

		window.onEachFrame = onEachFrame;
		window.onEachFrame(updateFPS);
		setInterval(function(){
			$("#lggrFps").html((1000/Lggr.frameTime).toFixed(1) + " fps");
		}, 1000);
	}
	
	function updateFPS()
	{
		var thisLoop = new Date;
		var thisFrameTime = thisLoop - Lggr.lastLoop;
		Lggr.frameTime += (thisFrameTime - Lggr.frameTime) / Lggr.filterStrength;
		Lggr.lastLoop = thisLoop;
	}
});